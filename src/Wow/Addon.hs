{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
module Wow.Addon (
    AddonInfo (..),
    AddonZip (..),
    extractAddon,
    AddonProvider (..),
) where


import           Codec.Archive.Zip
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Data.Text (Text)
import           Data.Time.Clock.POSIX
import           Network.HTTP.Req (MonadHttp)


data AddonInfo src = AddonInfo
    { name :: Text
    , downloadSource :: src
    , lastUpdated :: POSIXTime
    } deriving (Show)


newtype AddonZip = AddonZip
    { zipFilePath :: FilePath
    } deriving (Show)


class AddonProvider p where
    -- 'MonadBaseControl' for lifted concurrency capabilities, e.g. multiple
    -- GETs
    getAddonInfo :: (MonadHttp m, MonadBaseControl IO m)
                 => Text -> m (Either String (AddonInfo p))
    downloadAddon :: (MonadHttp m, MonadBaseControl IO m)
                  => AddonInfo p -> AddonZip -> m ()


extractAddon :: (MonadIO m)
             => AddonZip -> FilePath -> m ()
extractAddon AddonZip{..} addonsDir =
    liftIO $ withArchive zipFilePath $
        unpackInto addonsDir
