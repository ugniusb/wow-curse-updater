{-# LANGUAGE Arrows #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Wow.CurseForge (
    CurseForge,
) where


import           Control.Concurrent.Async.Lifted (concurrently)
import           Control.Monad
import           Control.Monad.IO.Class
import           Data.ByteString.Char8 (unpack)
import qualified Data.ByteString.Lazy as L
import           Data.Time.Clock.POSIX
import qualified Data.Text as T
import           Data.Text (Text)
import           Network.HTTP.Req
import           Text.XML.HXT.Core
import           Text.XML.HXT.TagSoup
import           Wow.Addon


type CurseURL = Url 'Https


newtype CurseForge = CurseForge
    { unCurseForge :: CurseURL
    } deriving (Show)


baseURL :: CurseURL
baseURL = https "www.curseforge.com"


data BaseAddonInfo = BaseAddonInfo
    { baiName :: Text
    , baiLastUpdated :: POSIXTime
    } deriving (Show)


mkAddonUrl :: Text -> CurseURL
mkAddonUrl t = baseURL /: "wow" /: "addons" /: t


mkAddonDownloadUrl :: Text -> CurseURL
mkAddonDownloadUrl t = mkAddonUrl t /: "download"


basicGet :: (MonadHttp m)
         => Url scheme
         -> m (IOStateArrow s b XmlTree)
basicGet url = do
    resp <- req GET url NoReqBody bsResponse mempty
    return
        $ readString
            [ withTagSoup
            , withParseHTML yes
            , withWarnings no
            ]
        . unpack
        . responseBody
        $ resp


atTag :: (ArrowXml a) => String -> a XmlTree XmlTree
atTag t = deep (isElem >>> hasName t)


hasClass :: (ArrowXml a) => String -> a XmlTree XmlTree
hasClass c = hasAttrValue "class" (==c)


parseName :: (ArrowXml a) => a XmlTree String
parseName =
        atTag "h2"
    >>> hasClass "name"
    >>> getChildren
    >>> getText


parseLastUpdated :: (ArrowXml a) => a XmlTree (Either String POSIXTime)
parseLastUpdated =
        atTag "span"
    >>> hasClass "stats--last-updated"
    >>> atTag "abbr"
    >>> getAttrValue "data-epoch"
    >>> arr (fmap fromInteger . tryRead "missing last updated date")


parseDownloadUrl :: (ArrowXml a) => a XmlTree CurseURL
parseDownloadUrl =
        atTag "a"
    >>> hasClass "download__link"
    >>> getAttrValue "href"
    >>> arr (\x -> baseURL /: T.pack x)


listToEither :: a -> [b] -> Either a b
listToEither _ (x:_) = Right x
listToEither e _     = Left e


tryRead :: Read a => String -> String -> Either String a
tryRead msg = fmap fst . listToEither msg . reads


getWithParse :: (MonadHttp m)
             => CurseURL
             -> String
             -> IOSArrow XmlTree a
             -> m (Either String a)
getWithParse url msg parse = do
    rawXml <- basicGet url
    mi <- liftIO $ runX $ rawXml >>> parse
    return $ case mi of
        (x:_) -> Right x
        _     -> Left msg


getBaseInfo :: (MonadHttp m)
            => Text -> m (Either String BaseAddonInfo)
getBaseInfo addon =
    fmap join $
        getWithParse (mkAddonUrl addon) "missing base info" $
            proc x -> do
                name <- parseName -< x
                lastUpdated <- parseLastUpdated -< x
                returnA -< BaseAddonInfo (T.pack name) <$> lastUpdated


getDownloadUrl :: (MonadHttp m)
               => Text -> m (Either String CurseURL)
getDownloadUrl addon =
    fmap join $
         getWithParse (mkAddonDownloadUrl addon) "missing download URL" $
            proc x -> do
                url <- parseDownloadUrl -< x
                returnA -< Right url


instance AddonProvider CurseForge where
    getAddonInfo addon = do
        (mbai, mdl) <- concurrently (getBaseInfo addon) (getDownloadUrl addon)
        return $ do
            BaseAddonInfo name lastUpdated <- mbai
            downloadURL <- mdl
            let downloadSource = CurseForge downloadURL
            return AddonInfo{..}
    downloadAddon AddonInfo{..} AddonZip{..} = do
        let url = unCurseForge downloadSource
        resp <- req GET url NoReqBody lbsResponse mempty
        liftIO $ L.writeFile zipFilePath (responseBody resp)
