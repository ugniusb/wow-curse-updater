{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           Config
import           Control.Exception
import           Control.Concurrent
import           Control.Concurrent.Async.Lifted
import           Control.Monad.Identity
import           Data.Time.Clock.POSIX
import           Data.Time.Format
import           Data.Time.LocalTime
import           Data.Default (def)
import           Data.Text (Text, unpack)
import           Network.HTTP.Req
import           System.Directory
import           System.Exit
import           System.IO.Error
import           System.IO.Temp
import           Wow.Addon
import           Wow.CurseForge


type AddonInfo' = AddonInfo CurseForge
type WithError a = Either String a


newtype Lock = Lock (MVar ())


withLock :: Lock -> IO a -> IO a
withLock (Lock mvar) action =
    withMVar mvar $ const action


newLock :: IO Lock
newLock =
    Lock <$> newMVar ()


defaultConfig :: Config'
defaultConfig = Config
    { deleteOld = Identity False
    , addonDir  = Identity "Addons"
    , addons    = Identity []
    }


handleE :: (Applicative m)
        => (e -> m a) -> Either e a -> m a
handleE _ (Right val)    = pure val
handleE handler (Left e) = handler e


doOrDie :: (Exception e)
        => (e -> String) -> IO a -> IO a
doOrDie p action =
    try action >>= handleE (die . p)


killThisThread :: IO a
killThisThread = do
    tid <- myThreadId
    killThread tid
    return undefined


configFile :: FilePath
configFile = "config.yaml"


tempDirTemplate :: String
tempDirTemplate = "wow-addon-updated"


mapLeft :: (a -> b) -> Either a c -> Either b c
mapLeft f (Left x)  = Left $ f x
mapLeft _ (Right x) = Right x


-- for type inference
displayHttpExc :: HttpException -> String
displayHttpExc = displayException

displayIOExc :: IOException -> String
displayIOExc = displayException


getAddonInfo' :: Text -> IO (WithError AddonInfo')
getAddonInfo' addon = do
    -- using `let` instead of `<-` to run async
    let info = runReq def $ getAddonInfo addon
    -- flatten nested Either
    join . mapLeft displayHttpExc <$> try info


downloadAddon' :: AddonInfo' -> AddonZip -> IO (WithError ())
downloadAddon' ai az = do
    let zipped = runReq def $ downloadAddon ai az
    mapLeft displayHttpExc <$> try zipped


extractAddon' :: AddonZip -> FilePath -> IO (WithError ())
extractAddon' az dir =
    mapLeft displayIOExc <$> try (extractAddon az dir)


getAddon :: Lock -> FilePath -> FilePath -> Text -> IO ()
getAddon stdoutLock tempDirectory addonDirectory addon = do
    let addon' = unpack addon
        logOk msg =
            withLock stdoutLock $
                putStrLn $ concat
                    [ "INFO  : "
                    , addon'
                    , " : "
                    , msg
                    ]
        logDie msg err = do
            withLock stdoutLock $
                putStrLn $ concat
                    [ "ERROR : "
                    , addon'
                    , " : "
                    , msg
                    , ": "
                    , err
                    ]
            killThisThread
    (info :: AddonInfo') <- getAddonInfo' addon
        >>= handleE (logDie "getting addon info failed: ")
    localTimeZone <- getCurrentTimeZone
    let updatedAt =
            formatTime defaultTimeLocale "%F"
                . utcToLocalTime localTimeZone
                . posixSecondsToUTCTime
                $ lastUpdated info
    logOk $ "last updated " ++ updatedAt
    let az = AddonZip
            { zipFilePath = concat
                [ tempDirectory
                , "/"
                , addon'
                , ".zip"
                ]
            }
    downloadAddon' info az
        >>= handleE (logDie "download failed")
    logOk "download complete"
    extractAddon' az addonDirectory
        >>= handleE (logDie "zip extraction failed")
    logOk "extraction complete"


getConfig :: IO Config'
getConfig = do
    mpc <- fromFile configFile
    pc <- handleE die mpc
    return $ pc `withDefaults` defaultConfig


withTemp :: (FilePath -> IO a) -> IO a
withTemp proc = do
    systemTemp <- getTemporaryDirectory
    createDir CreateParents systemTemp
    withTempDirectory systemTemp tempDirTemplate proc


data CreateParents = CreateParents | NoCreateParents

createDir :: CreateParents -> FilePath -> IO ()
createDir par =
    createDirectoryIfMissing $ case par of
        CreateParents   -> True
        NoCreateParents -> False


ignoreExc :: (Exception e)
          => (e -> Bool) -> IO () -> IO ()
ignoreExc predicate =
    handleJust
        (\e -> if predicate e then Just () else Nothing)
        return


ignoreNotExists :: IO () -> IO ()
ignoreNotExists =
    ignoreExc $ isDoesNotExistErrorType . ioeGetErrorType



main :: IO ()
main = do
    Config
        { deleteOld = Identity doDeleteOld
        , addonDir  = Identity addonDirectory
        , addons    = Identity addonNames
    } <- getConfig
    let newAddonDir = addonDirectory ++ ".new"
        oldAddonDir = addonDirectory ++ ".old"

    stdoutLock <- newLock

    withTemp $ \tempDir -> do
        asyncs <- forM addonNames $ \addon ->
            async $ getAddon stdoutLock tempDir newAddonDir addon
        mapM_ waitCatch asyncs


    if doDeleteOld
        then do
            putStrLn $ "      : DELETING " ++ oldAddonDir
            doOrDie displayIOExc . ignoreNotExists
                $ removeDirectoryRecursive oldAddonDir
        else
            putStrLn $ "      : NOT deleting " ++ oldAddonDir


    putStrLn $ concat ["      : renaming ", addonDirectory, " -> ", oldAddonDir]
    doOrDie displayIOExc . ignoreNotExists
        $ renameDirectory addonDirectory oldAddonDir
    putStrLn $ concat ["      : renaming ", newAddonDir, " -> ", addonDirectory]
    doOrDie displayIOExc $ renameDirectory newAddonDir addonDirectory

    putStrLn "      : JOB'S DONE"
