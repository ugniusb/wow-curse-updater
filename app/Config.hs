{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
module Config (
    Config (..), Config', PartialConfig,
    withDefaults,
    fromFile,
) where

import           Control.Exception
import           Control.Monad.Identity
import           Data.Aeson.Types (typeMismatch)
import           Data.ByteString as B
import           Data.Function (on)
import           Data.Text (Text)
import           Data.Yaml


data Config f = Config
    { deleteOld :: f Bool
    , addonDir  :: f FilePath
    , addons    :: f [Text]
    }
deriving instance Show (Config Identity)
deriving instance Show (Config Maybe)


type Config' = Config Identity
type PartialConfig = Config Maybe


instance FromJSON (Config Maybe) where
    parseJSON (Object v) = Config
        <$> v .:? "delete old addons"
        <*> v .:? "addon directory"
        <*> v .:? "addons"
    parseJSON invalid = typeMismatch "Config" invalid


instance Semigroup (Config Maybe) where
    a <> b =  Config
        { deleteOld = (takeLeft `on` deleteOld) a b
        , addonDir  = (takeLeft `on` addonDir)  a b
        , addons    = (takeLeft `on` addons)    a b
        }


instance Monoid (Config Maybe) where
    mempty = Config
        { deleteOld = Nothing
        , addonDir  = Nothing
        , addons    = Nothing
        }


takeLeft :: Maybe a -> Maybe a -> Maybe a
takeLeft Nothing x = x
takeLeft x       _ = x


withDefaults :: PartialConfig -> Config' -> Config'
withDefaults pc d =
    let
        -- rewraps a field (acc) from Just to Identity
        -- or takes default
        ex :: (forall f. Config f -> f a)
           -> Identity a
        ex acc = maybe (acc d) Identity (acc pc)
    in Config
        { deleteOld = ex deleteOld
        , addonDir  = ex addonDir
        , addons    = ex addons
        }


fromFile :: FilePath -> IO (Either String PartialConfig)
fromFile path = do
    (bs' :: Either IOException ByteString) <- try $ B.readFile path
    return $ case bs' of
        Left e -> Left $ displayException e
        Right bs ->
            case decodeEither' bs of
                Left e -> Left $ prettyPrintParseException e
                Right pc -> Right pc
